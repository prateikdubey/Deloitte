package problem1;

import java.util.*;

public abstract class Arithmetic {
	
	int num1,num2,num3;
	
	public abstract int calculate();
	
	public void read() {
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter num1 : ");
		num1 = scan.nextInt();
		System.out.print("Enter num2 : ");
		num2 = scan.nextInt();
	}
	
	public void display() {
		read();
		System.out.println(calculate());
	}

	public Arithmetic() {
		this.num1 = 0;
		this.num2 = 0;
		this.num3 = 0;
	}
	

}
