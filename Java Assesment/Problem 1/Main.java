package problem1;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Arithmetic obj[];
		obj = new Arithmetic[5];
		
		obj[0] = new Addition();
		obj[1] = new Subtraction();
		obj[2] = new Multiplication();
		obj[3] = new Division();
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Enter choice : ");
		int ch = sc.nextInt();
		
		while(ch>0 && ch<4) {
			obj[ch-1].display();
			break;
		}
		
	}

}
