1.*Write a query which will display the customer id, account type they hold, their account number and bank name.

=> select c.CUSTOMER_ID, a.ACCOUNT_TYPE, r.REFERENCE_ACC_NO, b.BANK_NAME  from CUSTOMER_PERSONAL_INFO c, ACCOUNT_INFO a, CUSTOMER_REFERENCE_INFO r, BANK_INFO b where c.CUSTOMER_ID=a.CUSTOMER_ID and a.CUSTOMER_ID=r.CUSTOMER_ID and a.IFSC_CODE=b.IFSC_CODE;

2.*Write a query which will display the customer id, account type and the account number of HDFC customers who registered after 12-JAN-2012 and before 04-APR-2012.

=> select a.CUSTOMER_ID, a.ACCOUNT_TYPE, r.REFERENCE_ACC_NO from  ACCOUNT_INFO a, CUSTOMER_REFERENCE_INFO r, BANK_INFO b where a.CUSTOMER_ID=r.CUSTOMER_ID and b.BANK_NAME='HDFC' and a.REGISTRATION_DATE BETWEEN TO_DATE('2012/01/12', 'YYYY/MM/DD') AND TO_DATE('2012/04/04', 'YYYY/MM/DD');

3.*Write a query which will display the customer id, customer name, account no, account type and bank name where the customers hold the account.

=> SELECT CUST.CUSTOMER_ID ID, CUST.CUSTOMER_NAME NAME, ACC.ACCOUNT_NO "ACC. NO.", ACC.ACCOUNT_TYPE TYPE, BANK.BANK_NAME FROM CUSTOMER_PERSONAL_INFO CUST INNER JOIN ACCOUNT_INFO ACC ON (CUST.CUSTOMER_ID = ACC.CUSTOMER_ID) INNER JOIN BANK_INFO BANK ON (BANK.IFSC_CODE = ACC.IFSC_CODE);

4.*Write a query which will display the customer id, customer name, gender, marital status along with the unique reference string and
 sort the records based on customer id in descending order. <br/>
<br/><b>Hint:  </b>Generate unique reference string as mentioned below
:
<br/> CustomerName_Gender_MaritalStatus
<br/><b> Example, </b>
<br/> C-005           KUMAR              M                 SINGLE            KUMAR_M_SINGLE
<BR/> 
Use ""UNIQUE_REF_STRING"" as alias name for displaying the unique reference string."

=> SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, CUST.GENDER, CUST.MARITAL_STATUS, CUST.CUSTOMER_NAME||'_'||CUST.GENDER||'_'||CUST.MARITAL_STATUS "UNIQUE_REF_STRING"
FROM CUSTOMER_PERSONAL_INFO CUST ORDER BY CUST.CUSTOMER_ID DESC;

5.*Write a query which will display the account number, customer id, registration date, initial deposit amount of the customer
 whose initial deposit amount is within the range of Rs.15000 to Rs.25000.

=> select account_no, customer_id, registration_date, initial_deposit amount from  ACCOUNT_INFO where initial_deposit BETWEEN 15000 AND 25000;

6.*Write a query which will display customer id, customer name, date of birth, guardian name of the customers whose name starts with 'J'.

=> select customer_id, customer_name, date_of_birth, guardian_name from CUSTOMER_PERSONAL_INFO where customer_name like 'J%';

7.*Write a query which will display customer id, account number and passcode. 
<br/>
Hint:  To generate passcode, join the last three digits of customer id and last four digit of account number.
 
<br/>Example
<br/>C-001                   1234567898765432                0015432
<br/>Use ""PASSCODE"" as alias name for displaying the passcode."

=> select customer_id, account_no, (substr(customer_id,-3)||substr(account_no,-4)) as PASSCODE from ACCOUNT_INFO;

8.*Write a query which will display the customer id, customer name, date of birth, Marital Status, Gender, Guardian name, 
contact no and email id of the customers whose gender is male 'M' and marital status is MARRIED.

=> SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, CUST.DATE_OF_BIRTH, CUST.MARITAL_STATUS, CUST.GENDER, CUST.GUARDIAN_NAME, CUST.CONTACT_NO, CUST.MAIL_ID
FROM CUSTOMER_PERSONAL_INFO CUST WHERE UPPER(CUST.GENDER)='M' AND UPPER(CUST.MARITAL_STATUS)='MARRIED';

9.*Write a query which will display the customer id, customer name, guardian name, reference account holders name of the customers 
who are referenced / referred by their 'FRIEND'.

=> SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, CUST.GUARDIAN_NAME, REF.REFERENCE_ACC_NAME FROM CUSTOMER_PERSONAL_INFO CUST INNER JOIN CUSTOMER_REFERENCE_INFO REF ON (CUST.CUSTOMER_ID = REF.CUSTOMER_ID AND REF.RELATION='FRIEND');

10.*Write a query to display the customer id, account number and interest amount in the below format with INTEREST_AMT as alias name
 Sort the result based on the INTEREST_AMT in ascending order.  <BR/>Example: 
$5<BR/>Hint: Need to prefix $ to interest amount and round the result without decimals.

=> SELECT ACC.CUSTOMER_ID, ACC.ACCOUNT_NO, '$'||CEIL(ACC.INTEREST) INTEREST_AMT
FROM ACCOUNT_INFO ACC
ORDER BY ACC.INTEREST;
 
11.*Write a query which will display the customer id, customer name, account no, account type, activation date,
 bank name whose account will be activated on '10-APR-2012'

=> SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, ACC.ACCOUNT_NO, ACC.ACCOUNT_TYPE, ACC.ACTIVATION_DATE, BANK.BANK_NAME FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN ACCOUNT_INFO ACC
ON (CUST.CUSTOMER_ID = ACC.CUSTOMER_ID AND ACC.ACTIVATION_DATE=TO_DATE('10-APR-2012','DD-MON-YYYY'))
INNER JOIN BANK_INFO BANK
ON (ACC.IFSC_CODE = BANK.IFSC_CODE);

12.*Write a query which will display account number, customer id, customer name, bank name, branch name, ifsc code
, citizenship, interest and initial deposit amount of all the customers.

=> SELECT ACC.ACCOUNT_NO, CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, BANK.BANK_NAME, BANK.BRANCH_NAME,
BANK.IFSC_CODE, CUST.CITIZENSHIP, ACC.INTEREST, ACC.INITIAL_DEPOSIT
FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN ACCOUNT_INFO ACC
ON (CUST.CUSTOMER_ID = ACC.CUSTOMER_ID)
INNER JOIN BANK_INFO BANK
ON (ACC.IFSC_CODE = BANK.IFSC_CODE);

13.*Write a query which will display customer id, customer name, date of birth, guardian name, contact number,
 mail id and reference account holder's name of the customers who has submitted the passport as an identification document.

=> SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, CUST.DATE_OF_BIRTH, CUST.GUARDIAN_NAME, CUST.CONTACT_NO, CUST.MAIL_ID, REF.REFERENCE_ACC_NAME
FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN CUSTOMER_REFERENCE_INFO REF
ON (CUST.CUSTOMER_ID = REF.CUSTOMER_ID)
WHERE UPPER(CUST.IDENTIFICATION_DOC_TYPE)='PASSPORT';

14.*Write a query to display the customer id, customer name, account number, account type, initial deposit, 
interest who have deposited maximum amount in the bank.

=> SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, ACC.ACCOUNT_NO, ACC.ACCOUNT_TYPE, ACC.INITIAL_DEPOSIT, ACC.INTEREST 
FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN ACCOUNT_INFO ACC
ON (CUST.CUSTOMER_ID=ACC.CUSTOMER_ID)
WHERE CUST.CUSTOMER_ID = (
SELECT CUSTOMER_ID 
FROM ACCOUNT_INFO 
WHERE INITIAL_DEPOSIT=(
SELECT MAX(INITIAL_DEPOSIT) 
FROM ACCOUNT_INFO));

15.*Write a query to display the customer id, customer name, account number, account type, interest, bank name 
and initial deposit amount of the customers who are getting maximum interest rate.

=> SELECT A.CUSTOMER_ID,A.CUSTOMER_NAME,B.ACCOUNT_NO,B.ACCOUNT_TYPE,B.INTEREST,C.BANK_NAME,B.INITIAL_DEPOSIT
FROM CUSTOMER_PERSONAL_INFO A 
INNER JOIN ACCOUNT_INFO B ON(A.CUSTOMER_ID=B.CUSTOMER_ID)
INNER JOIN BANK_INFO C ON(B.IFSC_CODE=C.IFSC_CODE)
WHERE A.CUSTOMER_ID = (SELECT CUSTOMER_ID FROM ACCOUNT_INFO WHERE INTEREST=(SELECT MAX(INTEREST) FROM ACCOUNT_INFO));

16.Write a query to display the customer id, customer name, account no, bank name, contact no 
and mail id of the customers who are from BANGALORE.

=> SELECT CUST.CUSTOMER_ID, CUST.CUSTOMER_NAME, ACC.ACCOUNT_NO, BANK.BANK_NAME, CUST.CONTACT_NO, CUST.MAIL_ID FROM CUSTOMER_PERSONAL_INFO CUST
INNER JOIN ACCOUNT_INFO ACC
ON (ACC.CUSTOMER_ID = CUST.CUSTOMER_ID)
INNER JOIN BANK_INFO BANK
ON (BANK.IFSC_CODE = ACC.IFSC_CODE)
WHERE INSTR(CUST.ADDRESS,'BANGALORE')>0;

17.Write a query which will display customer id, bank name, branch name, ifsc code, registration date, 
activation date of the customers whose activation date is in the month of march (March 1'st to March 31'st).

=> select a.customer_id, b.bank_name, b.branch_name, b.ifsc_code, a.registration_date, a.activation_date from ACCOUNT_INFO a, BANK_INFO b where a.IFSC_CODE=b.IFSC_CODE AND EXTRACT(month FROM a.activation_date)=3;

18.Write a query which will calculate the interest amount and display it along with customer id, customer name, 
account number, account type, interest, and initial deposit amount.<BR>Hint :Formula for interest amount, 
calculate: ((interest/100) * initial deposit amt) with column name 'interest_amt' (alias)

=> select ((b.interest/100) * b.initial_deposit) as interest_amt, a.customer_id, a.customer_name,
b.account_no, b.account_type, b.interest, b.initial_deposit 
from customer_personal_info a, account_info b
where a.customer_id = b.customer_id;  

19.Write a query to display the customer id, customer name, date of birth, guardian name, contact number, 
mail id, reference name who has been referenced by 'RAGHUL'.

=> select c.customer_id, c.customer_name, c.date_of_birth, c.guardian_name, c.contact_no, c.mail_id, r.reference_acc_name from CUSTOMER_PERSONAL_INFO c, CUSTOMER_REFERENCE_INFO r where c.CUSTOMER_ID=r.CUSTOMER_ID and r.REFERENCE_ACC_NAME='RAGHUL';

20."Write a query which will display the customer id, customer name and contact number with ISD code of 
all customers in below mentioned format.  Sort the result based on the customer id in descending order. 
<BR>Format for contact number is :  
<br/> ""+91-3digits-3digits-4digits""
<br/> Example: +91-924-234-2312
<br/> Use ""CONTACT_ISD"" as alias name."

=> select customer_id, customer_name, contact_no, ('+91-' ||SUBSTR(contact_no, 1, 3)|| '-' || SUBSTR(contact_no, 4, 3) || '-' || SUBSTR(contact_no, 7, 4)) as CONTACT_ISD from CUSTOMER_PERSONAL_INFO;

21.Write a query which will display account number, account type, customer id, customer name, date of birth, guardian name, 
contact no, mail id , gender, reference account holders name, reference account holders account number, registration date, 
activation date, number of days between the registration date and activation date with alias name "NoofdaysforActivation", 
bank name, branch name and initial deposit for all the customers.

=> select a.account_no, a.account_type, a.customer_id, c.customer_name, c.date_of_birth, c.guardian_name, c.contact_no, c.mail_id , c.gender, r.reference_acc_name, r.reference_acc_no, a.registration_date, a.activation_date, TRUNC( activation_date ) - TRUNC( registration_date ) AS "NoofdaysforActivation", b.bank_name, b.branch_name from  CUSTOMER_PERSONAL_INFO c, ACCOUNT_INFO a, CUSTOMER_REFERENCE_INFO r, BANK_INFO b where c.CUSTOMER_ID=a.CUSTOMER_ID and a.CUSTOMER_ID=r.CUSTOMER_ID and a.IFSC_CODE=b.IFSC_CODE;

22."Write a query which will display customer id, customer name,  guardian name, identification doc type,
 reference account holders name, account type, ifsc code, bank name and current balance for the customers 
who has only the savings account. 
<br/>Hint:  Formula for calculating current balance is add the intital deposit amount and interest
 and display without any decimals. Use ""CURRENT_BALANCE"" as alias name."

=> SELECT C.CUSTOMER_ID, C.CUSTOMER_NAME,  C.GUARDIAN_NAME, C.IDENTIFICATION_DOC_TYPE, R.REFERENCE_ACC_NAME, A.ACCOUNT_TYPE, B.IFSC_CODE, B.BANK_NAME, 
TRUNC (A.INITIAL_DEPOSIT+((A.INITIAL_DEPOSIT*INTEREST)/100)) AS "CURRENT_BALANCE" 
FROM CUSTOMER_PERSONAL_INFO C, ACCOUNT_INFO A, CUSTOMER_REFERENCE_INFO R, BANK_INFO B
WHERE C.CUSTOMER_ID=A.CUSTOMER_ID
AND A.CUSTOMER_ID=R.CUSTOMER_ID
AND A.IFSC_CODE=B.IFSC_CODE
AND UPPER(A.ACCOUNT_TYPE)='SAVINGS';

23."Write a query which will display the customer id, customer name, account number, account type, interest, initial deposit;
 <br/>check the initial deposit,<br/> if initial deposit is 20000 then display ""high"",<br/> if initial deposit is 16000 display 'moderate'
,<br/> if initial deposit is 10000 display 'average', <br/>if initial deposit is 5000 display 'low', <br/>if initial deposit is 0 display
 'very low' otherwise display 'invalid' and sort by interest in descending order.<br/>
Hint: Name the column as ""Deposit_Status"" (alias). 
<br/>Strictly follow the lower case for strings in this query."

=> select a.customer_id,a.customer_name,b.account_no,b.account_type,b.interest,b.initial_deposit,
case when b.initial_deposit = 20000 then'high'
when b.initial_deposit = 16000 then'moderate'
when b.initial_deposit = 10000 then'average'
when b.initial_deposit = 5000 then'low'
when b.initial_deposit = 0 then'very low'
else 'invalid' end as "Deposit_Status" from customer_personal_info a inner join account_info b on(a.customer_id=b.customer_id)
order by b.interest desc;

24."Write a query which will display customer id, customer name,  account number, account type, bank name, ifsc code, initial deposit amount
 and new interest amount for the customers whose name starts with ""J"". 
<br/> Hint:  Formula for calculating ""new interest amount"" is 
if customers account type is savings then add 10 % on current interest amount to interest amount else display the current interest amount.
 Round the new interest amount to 2 decimals.<br/> Use ""NEW_INTEREST"" as alias name for displaying the new interest amount.
<br/>Example, Assume Jack has savings account and his current interest amount is 10.00, then the new interest amount is 11.00 i.e (10 + (10 * 10/100)). 

=> select a.customer_id,a.customer_name ,b.account_no,b.account_type,c.bank_name,b.ifsc_code,'$'||b.initial_deposit,ROUND(DECODE(b.account_type,'SAVINGS',(b.interest + (b.interest*b.interest/100)),b.interest),2) as NEW_INTEREST from customer_personal_info a , account_info b ,bank_info c where (a.customer_id = b.customer_id and b.ifsc_code=c.ifsc_code and a.customer_name like 'J%');

25.Write query to display the customer id, customer name, account no, initial deposit, tax percentage as calculated below.
<BR>Hint: <BR>If initial deposit = 0 then tax is '0%'<BR>If initial deposit &lt;= 10000 then tax is '3%' 
<BR>If initial deposit &gt; 10000 and initial deposit &lt; 20000 then tax is '5%' <BR>If initial deposit &gt;= 20000 and
 initial deposit&lt;=30000 then tax is '7%' <BR>If initial deposit &gt; 30000 then tax is '10%' <BR>Use the alias name 'taxPercentage'

=> select a.customer_id, a.customer_name, b.account_no, b.initial_deposit,
  case when b.initial_deposit = 0 then '0%'
  when b.initial_deposit <= 10000 then '3%'
  when b.initial_deposit > 10000 and b.initial_deposit < 20000 then '5%'
  when b.initial_deposit >= 20000 and b.initial_deposit <= 30000 then '7%'
  else '10%' End "tax percentage"
  from customer_personal_info a, account_info b
  where a.customer_id = b.customer_id;
